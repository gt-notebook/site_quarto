---
title: "_Bibliothèque Zotero [WORK IN PROGRESS]_"
date: 04/24/2023
author:
- gt notebook
about:
  template: marquee
published-title: Date de séance
author-title: Auteurs
---

[\> retour aux activités en cours](../posts.qmd)

::: columns
::: column
<iframe class="slide" src="https://hackmd.io/vbgJ_d58Sdevzz7P-Dlhkg?view">

</iframe>
:::

::: {.column width="30%"}
<ul class="metaslide">

-   <i class="bi bi-person-fill"> **gt notebook**</i>
-   <i class="bi bi-file-earmark-slides"></i> Accès à la [bibliothèque de groupe](https://www.zotero.org/groups/4416056/gt-notebooks/library)
-   <i class="bi bi-github"></i> Accès au dépôt [GitLab](https://gitlab.huma-num.fr/gt-notebook/zotero)

</ul>
:::
:::
